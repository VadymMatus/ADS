#ifndef TURN_HPP
#define TURN_HPP

#include <iostream>

template <class C>
class Queue
{
  private:
    /**
	 * @brief Структура ячейки списка.
	 */
    struct Cell
    {
        Cell(C value)
        {
            this->value = value;
        }

        Cell()
        {
        }

        C value;           // значение одной ячейки списка
        Cell *next = NULL; // указатель на следующюю ячейку
    };

    Cell *cell_beg = NULL,
         *cell_end = NULL;
    size_t cell_size = 0;

  public:
    Queue();
    ~Queue();

    /**
     * @brief Добавление в очередь нового элемента.
     * 
     * @param value Значение, которое нужно добавить.
     */
    void push(C value);

    /**
     * @brief Функция вернёт размер очереди.
     * 
     * @return size_t Размер очереди
     */
    size_t size();

    /**
     * @brief Вернёт последний елемент в очереде.
     * 
     * @return C Последний елемент в очереди 
     */
    C back();

    /**
     * @brief Вернёт ведущий елемент в очереде.
     * 
     * @return C Ведущий елемент в очереди 
     */
    C front();

    /**
     * @brief Удаление первого элемента очереди.
     */
    void pop();
};

#endif // !TURN_HPP