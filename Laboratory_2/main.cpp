#include <iostream>
#include "../pz17p1.hpp"
#include "Queue.hpp"
#include "Stack.hpp"

/**
 * @brief Функция вызываеться после сообщения для
 * какого-либо подтверждения от пользователя. 
 * 
 * @return true Согласился 
 * @return false Отказался
 */
bool agreement();

void push(Queue<float> *queue);

void queue_processing(Queue<float> *queue, Stack<float> *stack);

void stack_processing(Stack<float> *stack);

int main(int argc, char const *argv[])
{
    Queue<float> queue;
    Stack<float> stack(20);

    pz::Menu menu(new std::string[5]{"Добавить новый елемент",
                                     "Обработка очереди",
                                     "Обработка стека",
                                     "Выход"});

    do
    {
        system("clear");
        switch (menu.run())
        {
        case 1:
            push(&queue);
            break;
        case 2:
            system("clear");
            queue_processing(&queue, &stack);
            break;
        case 3:
            system("clear");
            stack_processing(&stack);
            break;
        case 4:
            break;
        }
    } while (menu.get_last_selected() != 4);

    return 0;
}

void push(Queue<float> *queue)
{
    system("clear");

    float value = 0.0;

    std::cout << "Введите число \033[1;34m-> \033[0m";
    while (!(std::cin >> value))
    {
        std::cin.clear();
        while (std::cin.get() != '\n')
            continue;

        std::cout << "\a\033[1;31mFatal Error!\033[0m Введите число!\n"
                  << "\rВведите число \033[1;34m-> \033[0m";
    }

    while (std::cin.get() != '\n')
        continue;

    queue->push(value);
    std::cout << "\nПродолжить(Y/n)?: ";

    if (agreement()) push(queue);
}

void queue_processing(Queue<float> *queue, Stack<float> *stack)
{
    if (queue->size() == 0)
    {
        std::cout << "Стек пуст!" << std::endl;
        pz::_getch();
        return;
    }

    float x(0), y(0);
    auto size = queue->size();

    std::cout << "Диапазон обработки[x;y]: ";
    while (!(std::cin >> x >> y))
    {
        while (std::cin.get() != '\n')
            continue;

        std::cout << "Повторите ввод [x;y]:";
    }

    while (std::cin.get() != '\n')
        continue;

    for (size_t i(0); i < size; i++)
    {
        if (queue->front() > x && queue->front() < y)
            stack->push(queue->front());

        queue->pop();
    }

    std::cout << "Елементы входящие в диапазон занесены в стек ..." << std::endl;
    pz::_getch();
}

void stack_processing(Stack<float> *stack)
{
    std::cout << "Размер стека -> " << stack->getTop() << std::endl;
    pz::_getch();
}

bool agreement()
{
    char YesNo;
    bool flag(true);

    std::cin.get(YesNo);

    if (YesNo == '\n')
        return flag;
    else if (YesNo == 'N' or YesNo == 'n')
        flag = false;
    else if (YesNo == 'Y' or YesNo == 'y')
        flag = true;
    else
        flag = false;

    std::cin.clear();
    while (std::cin.get() != '\n')
        continue;

    return flag;
}