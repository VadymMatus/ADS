#include <iostream>
#include "Tree.hpp"
#include "../pz17p1.hpp"

int main(int argv, char *argc[])
{
    Tree<int> tree;
    pz::Menu menu(new std::string[7]{"Создать дерево вручную",
                                     "Автосоздание дерева",
                                     "Вывод дерева вертикально",
                                     "Вывод дерева горизонтально",
                                     "Вывод минимума и максимума",
                                     "Выход"});

    do
    {
        system("clear");
        switch (menu.run())
        {
        case 1:
            tree.create();
            getchar();
            break;
        case 2:
            tree.fill();
            std::cout << std::endl << "Дерево созданно ... ";
            getchar();
            break;
        case 3:
            system("clear");
            tree.printv();
            getchar();
            break;
        case 4:
            system("clear");
            tree.printh();
            getchar();
            break;
        case 5:
            system("clear");
            tree.print_min_max();
            getchar();
            break;
        case 6:
            break;
        }

    } while (menu.get_last_selected() != 6);

    return EXIT_SUCCESS;
}