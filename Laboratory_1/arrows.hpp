#ifndef ARROWS_HPP
#define ARROWS_HPP

#include <iostream>
#include <termios.h>
#include <unistd.h>

/**
 * @brief Перечисление для удобной работы с стрелками
 * на клавиатуре.
 */
enum
{
    KEY_UP = 183,
    KEY_DOWN = 184,
    KEY_LEFT = 186,
    KEY_RIGHT = 185,
    ENTER = 10
};

/**
 * @brief Функция считывает один символ из стандартного потока
 * ввода, без нажатия на <Enter>.
 * 
 * @return int ASSCI код введеного символа
 */
int mygetch();

/**
 * @brief Функция скрывает курсор в терминале.
 * 
 * @param flag скрыть курсор - false, показать курсор - true  
 */
void ShowCursor(bool flag);

#endif // ARROWS_HPP
