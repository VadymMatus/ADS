#include "arrows.hpp"

int mygetch()
{
    struct termios oldt, newt;
    int ch;
    tcgetattr(STDIN_FILENO, &oldt);
    newt = oldt;
    newt.c_lflag &= ~(ICANON | ECHO);
    tcsetattr(STDIN_FILENO, TCSANOW, &newt);
    ch = getchar();

    if (ch == ENTER)
        return ch;
    else if (ch != 27)
        return 0;

    ch += getchar() + getchar();
    tcsetattr(STDIN_FILENO, TCSANOW, &oldt);

    return ch;
}

void ShowCursor(bool flag)
{
    system("tput reset ");

    system((flag) ? "tput cnorm" : "tput civis");
}
