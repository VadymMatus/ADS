#include <iostream>
#include "List.hpp"
#include "arrows.hpp"

/**
 * @brief Основное меню программы.
 * Управление реализуеться с помощью стрелок на клавиатуре.
 * 
 * @return int Выбраный пункт меню [1-]
 */
int menu();

/**
 * @brief Функция реализует ввод и запись елемента
 * в начало списка.
 * 
 * @param list Список пользователя 
 */
int push_start(List<int> *list);

/**
 * @brief Функция вызываеться после сообщения для
 * какого-либо подтверждения от пользователя. 
 * 
 * @return true Согласился 
 * @return false Отказался
 */
bool agreement();

/**
 * @brief Функция выводит всё содержиое списка.
 * 
 * @param list Объект списка
 */
void show_list(List<int> *list);

/**
 * @brief Функция заменяет в списке все негативные 
 * елементы на их подуля.
 * 
 * @param list Объект списка
 */
void replacement_negative_num(List<int> *list);

/**
 * @brief Функция удаляет со списка все положительные елементы.
 * 
 * @param list Обект всписка
 */
void remove_positive_elements(List<int> *list);

int main(int argc, char const *argv[])
{
    bool exit(false);
    List<int> list;

    do
    {
        switch (menu())
        {
        case 1: // Добавление елементов в начало списка
            system("clear");
            push_start(&list);
            mygetch();
            break;
        case 2: // Просмотр всех елементов списка
            system("clear");
            show_list(&list);
            std::cout << "\033[1;33m<Enter>\033[0m";
            mygetch();
            break;
        case 3: // Очистить список
            list.clear();
            std::cout << "Список пуст ..." << std::endl;
            std::cout << "\n\033[1;33m<Enter>\033[0m";
            mygetch();
            break;
        case 4: // Задание №1
            system("clear");
            replacement_negative_num(&list);
            break;
        case 5: // Задание №2
            system("clear");
            remove_positive_elements(&list);
            break;
        case 6: // выход
            exit = true;
            std::cout << "\033[1;31mExit!\033[0m Завершено пользователем!\033[2;33m<Enter>...\033[0m" << std::endl;
            break;
        default:
            std::cout << "\a\033[1;31mFatal error!\033[0m Неизвестная ошибка!";
        }

    } while (exit == false);

    return 0;
}

int menu()
{
    int answer(0);            // ответ пользователя на меню
    static int8_t counter(1); // счётчик для меню

    ShowCursor(false); // скрываю курсор

    do
    {
        system("clear");

        std::cout << "   \033[1;33m<<< М Е Н Ю >>>\033[0m\n\n"
                  << ((counter == 1) ? "\033[1;31m-> \033[1;31m" : "   ") << "Добавление елементов в начало списка"
                  << ((counter == 1) ? "\033[1;31m <-\033[0m" : "   ") << '\n'
                  << ((counter == 2) ? "\033[1;31m-> \033[1;31m" : "   ") << "Просмотр всех елементов списка"
                  << ((counter == 2) ? "\033[1;31m <-\033[0m" : "   ") << '\n'
                  << ((counter == 3) ? "\033[1;31m-> \033[1;31m" : "   ") << "Очистить список"
                  << ((counter == 3) ? "\033[1;31m <-\033[0m" : "   ") << '\n'
                  << ((counter == 4) ? "\033[1;31m-> \033[1;31m" : "   ") << "Задание №1"
                  << ((counter == 4) ? "\033[1;31m <-\033[0m" : "   ") << '\n'
                  << ((counter == 5) ? "\033[1;31m-> \033[1;31m" : "   ") << "Задание №2"
                  << ((counter == 5) ? "\033[1;31m <-\033[0m" : "   ") << '\n'
                  << ((counter == 6) ? "\033[1;31m-> \033[1;31m" : "   ") << "Выйти"
                  << ((counter == 6) ? "\033[1;31m <-\033[0m" : "   ") << '\n'
                  << "\n\033[0;35mУправление\033[1;33m(↑↓)\033[0;35m\n"
                  << "Выбор\033[1;33m<Enter>\033[0m";

        answer = mygetch();

        if (answer == KEY_DOWN and counter < 7)
            counter++;
        else if (answer == KEY_UP and counter > 0)
            counter--;

        /* Осуществление кругового выбора */
        if (answer == KEY_DOWN and counter == 7)
            counter = 1;
        else if (answer == KEY_UP and counter == 0)
            counter = 6;

    } while (answer != ENTER);

    ShowCursor(true); // показываю курсор

    return counter;
}

int push_start(List<int> *list)
{
    int value(0);
    std::cout << "\rВведите целое число \033[1;34m-> \033[0m";

    while (!(std::cin >> value))
    {
        std::cin.clear();
        while (std::cin.get() != '\n')
            continue;

        std::cout << "\a\033[1;31mFatal Error!\033[0m Введите число!\n"
                  << "\rВведите целое число \033[1;34m-> \033[0m";
    }

    list->push_start(value);

    std::cout << "\nПродолжить(y/N)?: ";

    if (agreement())
        push_start(list);

    return 0;
}

bool agreement()
{
    char YesNo;
    bool flag(false);

    std::cin.get(YesNo);

    if (YesNo == '\n')
        return flag;
    else if (YesNo == 'N' or YesNo == 'n')
        flag = false;
    else if (YesNo == 'Y' or YesNo == 'y')
        flag = true;
    else
        flag = false;

    std::cin.clear();
    while (std::cin.get() != '\n')
        continue;

    return flag;
}

void show_list(List<int> *list)
{
    std::cout << "\t\033[1;33m<<< Содержимое списка >>>\033[0m\n\n";

    if (list->size() == 0)
        std::cout << "Список пуст ..." << std::endl;

    for (size_t i(0); i < list->size(); i++)
        std::cout << "list[" << i << "] -> " << list->get(i) << std::endl;
}

void replacement_negative_num(List<int> *list)
{
    if (list->size() == 0)
    {
        std::cout << "Список пуст ...";
        mygetch();
        return;
    }

    std::cout << "Замена всех отрицательных елементов в списке на их модуля ... ";

    auto size = list->size();
    bool flag = true;

    for (size_t i(0); i < size; i++)
        if (list->get(i) < 0)
        {
            list->set(i, abs(list->get(i)));
            flag = false;
        }

    std::cout << (flag == true ? "\nВ списке нет отрицательных елементов ... "
                               : "готово");

    mygetch();
}

void remove_positive_elements(List<int> *list)
{
    if (list->size() == 0)
    {
        std::cout << "Список пуст ...";
        mygetch();
        return;
    }

    std::cout << "Удаление из списка всех положительных елементов ... ";

    auto size = list->size();

    for (size_t i(0); i < list->size(); i++)
        if (list->get(i) > 0)
        {
            list->remove(i);
            i = -1;
        }

    std::cout << (size == list->size() ? "В списке нету положительных елементов ... "
                                       : "готово");

    mygetch();
}