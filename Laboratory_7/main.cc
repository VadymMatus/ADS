#include <iostream>
#include <iomanip>
#include <algorithm>

int current;
using namespace std;

/**
 * @brief Функция ввода и проверки коректности ввода.
 *  Являеться обёрткой для упрощения ввода чесел,
 *  в случае если ввода строки заместь числа, выведет
 *  сообщение и попросит повторить ввод.
 *
 * @tparam Args Стандартный тип даных, не рекомендуеться
 *  использовать строки.
 * @tparam T Строковый тип данных.
 * @param args Переменые для ввода
 * @param message Сообщение в случае ошибки
 */
template <typename T, typename... Args>
void input_check(const T &message, Args &... args)
{
    static_assert(std::is_same<T, char>::value ||
                      std::is_same<T, std::string>::value ||
                      std::is_array<T>::value,
                  "For message, use string types!");

    std::cout << message;

    while (!(std::cin >> ... >> args))
    {
        std::cin.clear();
        cin.ignore(numeric_limits<streamsize>::max(), '\n'); // очистка буфера ввода

        std::cout << "Error: " << /* (std::is_same<T, std::string>::value ? message.c_str() : */ message /* ) */;
    }

    std::cin.ignore(numeric_limits<streamsize>::max(), '\n'); // очистка буфера ввода
}

int main()
{
    int VES[100][100] = {{0}}; // Матрица весов графа
    bool visited[100]; //Массив, содержащий единицы и нули для каждой вершины,
                       // visited[i]=0 - еще не найден кратчайший путь в i-ю вершину,
                       // visited[i]=1 - кратчайший путь в i-ю вершину уже найден

    int DlinaPuti[100];  //t[i] - длина кратчайшего пути от вершины s в i
    int PredVertex[100]; //h[i] - вершина, предшествующая i-й вершине
                         //на кратчайшем пути
    size_t count;
    int start; // Номер исходной вершины

    input_check("Ввести количество вершин в графе /> ", count);

    cout << "Заполните матрицу графа:" << endl;
    for (register size_t i(0); i < count; i++)
        for (register size_t j(i); j < count; j++)
            input_check([&] {
                return "VES[" + to_string(i+1) + "][" + to_string(j+1) + "]/> ";
            }(),VES[i][j]);

    // заполнение другой стороны симетрично
    for (register size_t i(0); i < count; i++)
        for (register size_t j(0); j < count; j++)
            VES[j][i] = VES[i][j];


    do
    {
        input_check("Введите стартовую вершину: ", start);
    } while (start > (count - 1) && start < 0);

    start -= 1; //так как массив начинается с 0 отнимаем от вводимой цифры 1

    for (register size_t end(0); end < count; end++)
    {
        // end = end; //цикл прогоняет алгоритм Флойда count-ое количество раз преврашая его в алгоритм Дейкстры
        if (end == start)
            continue; //исключаем просчет растояния между одной и той же точкой

        // Инициализируем начальные значения массивов
        for (register size_t j(0); j < count; j++)
        {
            DlinaPuti[j] = INT32_MAX; //Сначала все кратчайшие пути из s в i
                                      //равны бесконечности
            visited[j] = false;       // и нет кратчайшего пути ни для одной вершины
        }

        PredVertex[start] = 0; // s - начало пути, поэтому этой вершине ничего не предшествует
        DlinaPuti[start] = 0;  // Кратчайший путь из s в s равен 0
        visited[start] = true; // Для вершины s найден кратчайший путь
        current = start;       // Делаем s текущей вершиной

        while (true)
        {
            // Перебираем все вершины, смежные current, и ищем для них кратчайший путь
            for (register size_t j(0); j < count; j++)
            {
                if (VES[current][j] == 0)
                    continue; // Вершины j и current несмежные

                if (visited[j] == false && DlinaPuti[j] > DlinaPuti[current] + VES[current][j]) //Если для вершины 'j' еще не
                                                                                                //найден кратчайший путь
                                                                                                // и новый путь в 'j' короче чем
                                                                                                //старый, то
                {
                    DlinaPuti[j] = DlinaPuti[current] + VES[current][j]; //запоминаем более короткую длину пути в
                                                                         //массив t[и]
                    PredVertex[j] = current;                             //запоминаем, что current->j часть кратчайшего
                                                                         //пути из s->j
                }
            }

            // Ищем из всех длин некратчайших путей самый короткий
            int w = INT32_MAX;                               // Для поиска самого короткого пути
            current = -1;                                    // В конце поиска current - вершина, в которую будет
                                                             // найден новый кратчайший путь. Она станет
                                                             // текущей вершиной
            for (register size_t j(0); j < count; j++)       // Перебираем все вершины.
                if (visited[j] == false && DlinaPuti[j] < w) // Если для вершины не найден кратчайший
                                                             // путь и если длина пути в вершину 'j' меньше
                                                             // уже найденной, то
                {
                    current = j; // текущей вершиной становится 'j'-я вершина
                    w = DlinaPuti[j];
                }

            if (current == -1)
            {
                cout << "Нет пути из вершины " << start + 1 << " в вершину " << end + 1 << "." << endl;
                break;
            }

            if (current == end) // Найден кратчайший путь,
            {
                cout << "Кратчайший путь из вершины " << start + 1 << " в вершину " << end + 1 << ": ";

                int j = end;

                // std::reverse(&PredVertex[start], &PredVertex[end]);

                while (j != start){
                    cout << " " << j + 1;
                    j = PredVertex[j];
                }

                cout << " " << start + 1 << ". Длина пути - " << DlinaPuti[end] << endl;
                
                break;
            }

            visited[current] = true;
        }
    }

    return EXIT_SUCCESS;
}