#ifndef INPUT_HPP
#define INPUT_HPP

#include <iostream>
#include <sstream>
#include <vector>
#include <termios.h>
#include <unistd.h>
#include "enums.hpp"

namespace pz
{

/**
 * @brief Функция ввода и проверки коректности ввода.
 *  Являеться обёрткой для упрощения ввода чесел,
 *  в случае если ввода строки заместь числа, выведет
 *  сообщение и попросит повторить ввод.
 * 
 * @tparam T Стандартный тип даных, не рекомендуеться 
 *  использовать строки.
 * @param value Переменая с для ввода
 * @param message Сообщение в случае ошибки
 */
template <typename T>
void input_check(T &value, const char *message)
{
    while (!(std::cin >> value))
    {
        std::cin.clear();
        while (getchar() != '\n')
            continue;

        std::cout << message;
    }

    while (getchar() != '\n')
        continue;
}

#ifdef __MINGW32__ // для Windows
#endif             // __MINGW32__

#ifdef __linux__ // для Linux

/**
 * @brief Функция считывает один символ из стандартного потока
 * ввода, без нажатия на <Enter>.
 * 
 * @return int ASSCI код введеного символа
 */
int _getch();

/**
 * @brief Функция выводит цветной текст.
 * Функцию следует использовать в операторах вывода таких как std::cout;
 * после вызова функции весь последующий текст в консоле будет
 * отображен в указаном цвете и с указаными ефектами. Текст будет
 * выводится в цвете до тех пор, пока не будет вызвана функция fb_end()
 * Пример реализации:
 *   std::cout << pz::fground(pz::FG_RED) << "Hello World!!!";
 * 
 * @param color Константа из перечисления pz::Foreground. Цвет последующего текста.
 * @param effect Константа из перечисления pz::Effect. Ефекты последующего текста.
 * @return std::string Последовательность управляющих ASSCI символов.
 */
std::string fground(size_t color, size_t effect = pz::Effect::RESET);

/**
 * @brief Функция очищает всё что сделала функция pz::fgroud().
 * После вызова функции, весь последующий текст будет отображен
 * по умолчанию, пропадут все ефекты текта применяемые до етого.
 * Функцию следует использовать в операторах вывода таких как std::cout;
 * Пример реализации:
 *   std::cout <<  << pz::fb_end() << "Hello World!!!";
 * 
 * @return std::string Последовательность управляющих ASSCI символов.
 */
std::string fb_end();

#endif // __linux__

} // namespace pz

#endif // INPUT_HPP