#include "input.hpp"

std::string pz::fground(size_t color, size_t effect)
{
    std::stringstream ss;
    ss << "\033[" << effect << ";" << color << "m";

    return ss.str();
}

std::string pz::fb_end()
{
    std::stringstream ss;
    ss << "\033[0m";

    return ss.str();
}

int pz::_getch()
{
    struct termios newt, oldt;
    tcgetattr(STDIN_FILENO, &oldt);
    newt = oldt;
    newt.c_lflag &= ~(ICANON | ECHO);
    tcsetattr(STDIN_FILENO, TCSANOW, &newt); /* enable raw mode */

    int c = 0;

    for (;;) /* read_symbols */
    {
        c = getchar();
        if (c == 27) /* if ESC */
        {
            c = getchar();
            if (c == '[') /* ... [ */
            {
                c = getchar();
                break;
            }
        }
        break;
    }

    tcsetattr(STDIN_FILENO, TCSANOW, &oldt); /* return old config */
    return c;
}