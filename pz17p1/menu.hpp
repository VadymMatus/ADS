#ifndef MENU_HPP
#define MENU_HPP

#include "enums.hpp"
#include "input.hpp"
#include <iostream>
#include <string.h>
#include <vector>

namespace pz
{

#ifdef __MINGW32__ // для Windows
#endif             // __MINGW32__

#ifdef __linux__ // для Linux

class Menu
{
private:
  std::vector<std::string> menu; // пункты меню
  size_t size = 0,               // количество пунктов меню
      last = 0;
  std::string ptrl = "-> ", // левый указатель
      ptrr = " <-",         // правый указатель
      caption = "\t<<< М Е Н Ю >>>";
  bool clear; // очистка экрана перед каждым выводом

public:
  Menu(std::string *menu, bool clear = true);
  ~Menu();

  size_t run();
  size_t get_last_selected();

  void set_clear(bool clear);
  
  /**
   * @brief Функция скрывает курсор в терминале.
   * 
   * @param flag скрыть курсор - false, показать курсор - true  
   */
  void show_cursor(bool flag);
};

#endif

} // namespace pz

#endif // MENU_HPP